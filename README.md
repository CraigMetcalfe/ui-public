## Mimecast-UI

### Introduction

This sample project is an AngularJS 1.5 based application, which provides a useful dashboard to view data provided by a RESTful server.

To launch the UI client simply run:
```
npm start
```

This will launch the UI on ```localhost:8080``` and issues API requests to ```localhost:8081/api/```.

The default login is:
```
Username: mimecast
Password: password
```

### Project Structure

At a high level, the structure of this project is viewed as:
```
bootstrap
├── .idea/            (IntelliJ IDEA Project files)
├── dist/             (combined and minified JS, CSS and HTML files)
├── node_modules/     (project dependencies added by npm)
├── src/main/
│   ├── html/         (modular HTML views controlled by JS)
│   ├── images/       (images required by the HTML views)
│   ├── scripts/      (root for all Javascript files)
│   └── scss/         (root for all SCSS files)
│
├── package.json      (references build scripts and dependencies)
└── webpack.config.js (config for building the UI)
```

The ```script/``` folder is broken down into separate modules:

```
script/
├── config/      ('Config' files are run whilst the module is loading)
├── constants/   (Reusable constants to be used throughout the project)
├── controllers/ (Controllers usually define interaction with HTML views)
├── directives/  (Directives for re-usable components, tables, buttons, etc.)
├── run/         (Run-blocks used to 'kick-start' the application, loaded after services)
├── services/
│   ├── api/     (APIs used to communicate with the backend server)
│   └── model/   (Data models to pass around the UI)
│
├── 3rd-party.js (3rd party dependency definitions, exported for other files)
└── app.js       (The main JS file for the application, references dependant modules)
```
