import { _ } from '../3rd-party.js';

export default ngInject(function($scope, SpamApi, DoughnutChartModel, HorizontalBarChartModel) {

    const updateDonut = (data) => {
        const pieChartContext = document.getElementById("pieChart");

        let model = new DoughnutChartModel(data);
        model.title = 'HAM vs SPAM';
        model.showLegend = true;

        const myDoughnutChart = new Chart(pieChartContext, model.toData());

        pieChartContext.onclick = function(evt) {
            let activePoints = myDoughnutChart.getElementsAtEvent(evt);
            $scope.table.params.type = getClickedDataLabel(activePoints);
            $scope.table.params.category = null;
            refreshTable();
        };
    };

    const updateBarChart = (data) => {
        const barChartContext = document.getElementById("barChart");

        let model = new HorizontalBarChartModel(data);
        model.title = 'SPAM Categories';
        model.showLegend = false;

        const myBarChart = new Chart(barChartContext, model.toData());

        barChartContext.onclick = function(evt) {
            let activePoints = myBarChart.getElementsAtEvent(evt);
            $scope.table.params.category = getClickedDataLabel(activePoints);
            $scope.table.params.type = 'spam';
            refreshTable();
        };
    };

    const getClickedDataLabel = (activePoints) => {
        if (activePoints[0]) {
            let chartData = activePoints[0]['_chart'].config.data;
            let idx = activePoints[0]['_index'];
            return chartData.labels[idx];
        }
    };

    SpamApi.getSummary()
        .then((response) => {
            updateDonut(response.data.donut);
            updateBarChart(response.data.bar);
        });

    $scope.table = {
      data: {},
      currentPage: 0,
      params: {
          limit: 20,
          offset: 0,
          type: null,
          category: null
      }
    };

    const refreshTable = (params = {}) => {
        _.assign(params, $scope.table.params);
        params.offset = ($scope.table.currentPage * $scope.table.params.limit);

        SpamApi.getSpam(params)
            .then((response) => {
                $scope.table.data = response.data;
            });
    };

    refreshTable();

    $scope.previousPage = () => {
        if ($scope.table.currentPage > 0)
        {
            $scope.table.currentPage--;
        }

        refreshTable()
    };

    $scope.nextPage = () => {
        $scope.table.currentPage++;
        refreshTable()
    };
});