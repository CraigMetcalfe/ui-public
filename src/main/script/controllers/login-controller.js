import { _ } from '../3rd-party.js';

export default ngInject(function($scope, $state) {

    const DEFAULT_USER = 'mimecast';
    const DEFAULT_PASS = 'password';

    $scope.data = {
      username: '',
      password: '',
      showError: false
    };

    const validateUsername = (username) => {
        return !_.isEmpty(username) && _.toLower(username) === DEFAULT_USER;
    };

    const validatePassword = (password) => {
        return !_.isEmpty(password) && _.toLower(password) === DEFAULT_PASS;
    };

    $scope.onChange = function() {
        // Clear the incorrect username prompt
        $scope.data.showError = false;

        if (_.isEmpty($scope.data.username) && _.isEmpty($scope.data.password)) {
            $scope.signInForm.$setPristine();
        }
    };

    $scope.login = (form) => {
        if (validateUsername($scope.data.username) && validatePassword($scope.data.password)) {
            $scope.data.showError = false;
            $state.go('dashboard');
        }
        else {
            $scope.data.showError = true
        }
    };
});