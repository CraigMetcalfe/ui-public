export default ngInject(function($scope, SpamApi) {

    $scope.data = {
        showAlert: false,
        isSpam: false,
        message: '',
        probability: 0
    };

    const showSpamResult = (data) => {
        $scope.data.showAlert = true;
        $scope.data.isSpam = data.type === 'spam';
        $scope.data.message = data.message;
        $scope.data.probability = data.probability;
    };

    $scope.submitAnalysis = (form) => {

        SpamApi.analyseSpam($scope.form.message)
            .then((response) => showSpamResult(response.data));
    };

    $scope.dismissAlert = () => {
        $scope.data.showAlert = false;
    };

});