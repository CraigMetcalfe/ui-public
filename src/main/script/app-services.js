import app from './app-module.js';

import ChartModel from './services/model/chart-model.js'
app.factory('ChartModel', ChartModel);

import DoughnutChartModel from './services/model/doughnut-chart-model.js'
app.factory('DoughnutChartModel', DoughnutChartModel);

import HorizontalBarChartModel from './services/model/horizontal-bar-chart-model.js'
app.factory('HorizontalBarChartModel', HorizontalBarChartModel);

import SpamApi from './services/api/spam-api.js'
app.factory('SpamApi', SpamApi);