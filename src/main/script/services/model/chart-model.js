import {_} from '../../3rd-party.js';

export default ngInject(function() {

    const COLOUR_CHART = {
        meadowlark: '#ECDB54',
        cherryTomato: '#E94B3C',
        lightBlue: '#6F9FD8',
        chilliOil: '#944743',
        pinkLavendar: '#DBB1CD',
        bloomingDahlia: '#EC9787',
        arcadia: '#00A591',
        ultraViolet: '#6B5B95',
        emparador: '#6C4F3D',
        mauve: '#EADEDB'
    };

    const DEFAULT_OPTIONS = {
        responsive: false,
        legend: { display: false },
        title: { display: false, text: '' }
    };

    const DEFAULT_DATA = {
        labels: [ ],
        datasets: [ ]
    };

    let createDataSet = function(data) {
        return {
            data: _.map(data, 'value'),
            backgroundColor: _.takeRight(Object.values(COLOUR_CHART), data.length)
        };
    };

    return class ChartModel {
        constructor(type, data, options) {
            this.type = type;
            this.options = _.merge({}, DEFAULT_OPTIONS, options);
            this.data = _.merge({}, DEFAULT_DATA, { labels: _.map(data, 'label'), datasets: [ createDataSet(data) ]});
        }

        set title(title) {
            this.options.title = {
                display: true,
                text: title
            };
        };

        set showLegend(value) {
            this.options.legend.display = !!value;
        }

        toData() {
            return {
                type: this.type,
                data: this.data,
                options: this.options
            }
        }
    };
});