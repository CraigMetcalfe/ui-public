export default ngInject(function(ChartModel) {

    return class HorizontalBarChartModel extends ChartModel {
        constructor(data, options) {
            super('horizontalBar', data, options);
        }
    };
});