export default ngInject(function(ChartModel) {

    return class DoughnutChartModel extends ChartModel {
        constructor(data, options) {
            super('doughnut', data, options);
        }
    };
});