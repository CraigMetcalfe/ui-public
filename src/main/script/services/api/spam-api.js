import {_} from '../../3rd-party.js';

export default ngInject(function($http) {

    function getSpam(params = {}) {
        return $http.get('http://localhost:8081/api/spam', { params: _.merge({limit: 20, offset: 0}, params) });
    }

    function getSummary() {
        return $http.get('http://localhost:8081/api/spam/summary');
    }

    function analyseSpam(message) {
        return $http.post('http://localhost:8081/api/spam/analyse', { message: message });
    }

    return {
        analyseSpam,
        getSpam,
        getSummary,
    };
});