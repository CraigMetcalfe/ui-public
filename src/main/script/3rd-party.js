import $ from 'jquery';
import angular from 'angular';
import _ from 'lodash';
import moment from 'moment';
import d3 from 'd3';
import Chart from 'chart.js';
import Popper from 'popper.js';
import feather from 'feather-icons'

import 'angular-ui-router';
import 'angular-ui-bootstrap';
import 'angular-resource';
import 'angularjs-toaster';
// import 'bootstrap-sass';
import 'bootstrap';
import 'feather-icons/dist/feather.js'
import 'jasny-bootstrap/dist/js/jasny-bootstrap.js';
import 'jquery-ui-bundle';


export {
    angular,
    d3,
    $,
    _,
    moment,
    Chart,
    feather
};

// Hackity hack hack hack to get ng-annotate to understand ES6
// https://github.com/google/traceur-compiler/issues/206
window.ngInject = _.identity;