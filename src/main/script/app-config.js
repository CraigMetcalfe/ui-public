import app from './app-module.js';

//import AuthenticatedRoutes from 'config/authenticated-routes.js';
//appModule.config(authenticatedRoutes);

import unauthenticatedRoutes from './config/unauthenticated-routes.js';
app.config(unauthenticatedRoutes);

//import translation from 'config/translation.js';
//appModule.config(translation);