/**
 * Copyright 2014-2015 Sophos Limited. All rights reserved.
 *
 * 'Sophos' and 'Sophos Anti-Virus' are registered trademarks of Sophos Limited
 * and Sophos Group.  All other product and company names mentioned are
 * trademarks or registered trademarks of their respective owners.
 */
import { _ } from '../3rd-party.js';

export default ngInject(function($urlMatcherFactoryProvider, $urlRouterProvider, $stateProvider, $locationProvider) {

    const DEFAULT_ROUTE = '/';
    const DEFAULTS = { layout: 'unauthenticated', access: 'everyone' };

    function view(key) { return { templateUrl: 'views/pages/' + key + '.html' }; }

    function addState(name, url, ...options) {
        return $stateProvider.state(name, _.assign({ url }, DEFAULTS, ...options));
    }

    /**
    addState('error', '/error', view('error'));
    addState('loading', '/loading', view('loading'));
     */

    //$urlRouterProvider.otherwise(DEFAULT_ROUTE);
    $urlRouterProvider.otherwise('/login');

    $urlMatcherFactoryProvider.strictMode(false);

    // run/authentication.js will dispatch to other routes based on customer type
    $stateProvider.state('login', {
        url: '/login',
        controller: 'LoginController',
        templateUrl: 'html/views/login.html'
    });

    $stateProvider.state('index', {
        abstract: true,
        // url: '',
        views: {
            '@': {
                //controller: 'DashboardController',
                templateUrl: 'html/views/layout.html'
            },
            'top-nav-menu@index': {
                templateUrl: 'html/views/top-nav-menu.html'
            },
            'left-nav-menu@index': {
                controller: 'LeftNavMenuController',
                templateUrl: 'html/views/left-nav-menu.html'
            }
        }
    });

    $stateProvider.state('dashboard', {
        parent: 'index',
        url: '/dashboard',
        views: {
            'main@index': {
                controller: 'DashboardController',
                templateUrl: 'html/views/dashboard.html'
            }
        }
    });

    $stateProvider.state('training', {
        parent: 'index',
        url: '/training',
        views: {
           'main@index': {
               templateUrl: 'html/views/training.html',
               controller: 'TrainingController'
           }
        }
    });

    $stateProvider.state('analysis', {
        parent: 'index',
        url: '/analysis',
        views: {
            'main@index': {
                templateUrl: 'html/views/analysis.html',
                controller: 'AnalysisController'
            }
        }
    });

    $stateProvider.state('settings', {
        parent: 'index',
        url: '/settings',
        views: {
            'main@index': {
                templateUrl: 'html/views/settings.html',
                controller: 'SettingsController'
            }
        }
    });

    // html5Mode uses URL path changes instead of search hash for location changes
    $locationProvider.html5Mode({ enabled: true, requireBase: false });

});
