import './3rd-party.js';
import './app-constants.js';
import './app-config.js';
import './app-controllers.js';
import './app-directives.js';
import './app-services.js';
import './app-module.js';
import './app-run.js';

// CSS imports
import '../scss/app.scss';
import 'jquery-ui-bundle/jquery-ui.theme.css';
