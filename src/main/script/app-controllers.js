import app from './app-module.js';

import LoginController from './controllers/login-controller.js';
app.controller('LoginController', LoginController);

import DashboardController from './controllers/dashboard-controller.js';
app.controller('DashboardController', DashboardController);

import LeftNavMenuController from './controllers/left-nav-menu-controller.js'
app.controller('LeftNavMenuController', LeftNavMenuController);

import TrainingController from './controllers/tranining-controller.js'
app.controller('TrainingController', TrainingController);

import AnalysisController from './controllers/analysis-controller.js'
app.controller('AnalysisController', AnalysisController);

import SettingsController from './controllers/settings-controller.js'
app.controller('SettingsController', SettingsController);