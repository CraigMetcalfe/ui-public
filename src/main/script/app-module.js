import {_, angular} from './3rd-party.js';

/**
 * Providers: list of services required by the application, any service listed here is available app-wide.
 * More information - https://angular.io/guide/providers
 */
let providers = angular.module('app.providers', [
    'ngResource',                   // node_modules/angular-resource/angular-resource.js
    'ui.bootstrap',                 // node_modules/angular-bootstrap/ui-bootstrap-tpls.js
    'ui.router',                    // node_modules/angular-ui-router/release/angular-ui-router.js
    'toaster',                      // node_modules/angularjs-toaster/toaster.js
    //'feather-icons'
]);

let components = angular.module('app.components', [
    'app.providers'
]);

/**
 * 
 */
let config = angular.module('app.config', [
    'app.providers',
    'ui.router'                     // node_modules/angular-ui-router/release/angular-ui-router.js
]);

/**
 *
 */
let directives = angular.module('app.directives', [
    'app.providers',
    'ui.bootstrap',                 // node_modules/angular-bootstrap/ui-bootstrap-tpls.js
    'ui.router',                    // node_modules/angular-ui-router/release/angular-ui-router.js
    // 'feather-icons'
]);

/**
 *
 */
let controllers = angular.module('app.controllers', [
    'app.config',
    'app.providers',
    'ngResource',
    'ui.bootstrap',                 // node_modules/angular-bootstrap/ui-bootstrap-tpls.js
    'ui.router',                    // node_modules/angular-ui-router/release/angular-ui-router.js
    // 'feather-icons'
]);

let filters = angular.module('app.filters', [

]);

/**
 *
 */
let run = angular.module('app.run', [
    'app.config',
    'app.providers',
]);

/**
 *
 */
angular.module('app', [
    'app.providers',
    'app.config',
    'app.directives',
    'app.controllers',
    'app.run',
    'ngResource',
    'ui.router',                    // node_modules/angular-ui-router/release/angular-ui-router.js
    'ui.bootstrap',                 // node_modules/angular-bootstrap/ui-bootstrap-tpls.js
    'toaster',                      // node_modules/angularjs-toaster/toaster.js
    //'nvd3',                         // node_modules/angular-nvd3/dist/angular-nvd3.js
    // 'feather-icons'
]);

export default {
    component: _.bind(components.component, components),
    config: _.bind(config.config, config),
    constant: _.bind(providers.constant, providers),
    controller: _.bind(controllers.controller, controllers),
    directive: _.bind(directives.directive, directives),
    factory: _.bind(providers.factory, providers),
    filter: _.bind(filters.filter, filters),
    run: _.bind(run.run, run)
};